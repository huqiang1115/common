package com.bikini.common.pojo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="product")
public class Product {
	//根据驼峰命名定义属性
	
	/**
	 * 	数据库主键id
	 */
	private Integer id;
	/**
	 *	 商品Id
	 */
	@Id
	private String pId;
	/**
	 * 商品名称
	 */
	private String pName;
	/**
	 * 单个商品地址
	 */
	private String pAddr;
	/**
	 * 商品图片
	 */
	private String pImg;
	/**
	 * 商品价格
	 */
	private String pPrice;
	/**
	 * 店铺名称
	 */	
	private String pShop;
	/**
	 * 店铺链接地址
	 */
	private String pShopAddr;
	//getter setter
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getpId() {
		return pId;
	}
	public void setpId(String pId) {
		this.pId = pId;
	}
	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}
	public String getpAddr() {
		return pAddr;
	}
	public void setpAddr(String pAddr) {
		this.pAddr = pAddr;
	}
	public String getpImg() {
		return pImg;
	}
	public void setpImg(String pImg) {
		this.pImg = pImg;
	}
	public String getpPrice() {
		return pPrice;
	}
	public void setpPrice(String pPrice) {
		this.pPrice = pPrice;
	}
	public String getpShop() {
		return pShop;
	}
	public void setpShop(String pShop) {
		this.pShop = pShop;
	}
	public String getpShopAddr() {
		return pShopAddr;
	}
	public void setpShopAddr(String pShopAddr) {
		this.pShopAddr = pShopAddr;
	}
	@Override
	public String toString() {
		return "Product [id=" + id + ", pId=" + pId + ", pName=" + pName + ", pAddr=" + pAddr + ", pImg=" + pImg
				+ ", pPrice=" + pPrice + ", pShop=" + pShop + ", pShopAddr=" + pShopAddr + "]";
	}
	
}