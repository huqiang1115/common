package com.bikini.common.utils;
/**
 * Jd url processor
 *
 */
public class JdUrlUtil {

	//https://search.jd.com/Search?keyword=%E6%89%8B%E6%9C%BA&enc=utf-8&psort=3&page=3
	/*keyword:关键词（京东搜索框输入的信息）
     * enc：编码方式（可改动:默认UTF-8）
     * psort=3 表示搜索方式  默认按综合查询 不给psort值
     * page=分页（不考虑动态加载时按照基数分页，每一页30条，这里就不演示动态加载）注意：受京东商品个性化影响，准确率无法保障
     * */
	
	private static String JdUrlPre = "https://search.jd.com/Search?keyword=";
	private static String JdUrlSuf = "&enc=utf-8&psort=3&page=";
	/**
	 * url拼接
	 * @param keyWord 搜索的关键字
	 * @param page 页数
	 * @return url
	 */
	public static String getUrl(String keyWord,int page) {
		return JdUrlUtil.JdUrlPre+keyWord+JdUrlUtil.JdUrlSuf+page;
	}
}
